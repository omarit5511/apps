<?php

namespace App\Http\Controllers;

use App\Models\Group;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function create_group(Request $request){

        $validateData=$request->validate([
            'name'=> 'required'
        ]);

        if ($validateData){

        $group=Group::create(['name'=>$request->name]);
        if($group)
            return response()->json('create group successfully');

        return response()->json('create group failed');
        }
        return response()->json('please enter name of group');
    }

    public function getAllGroups(){
        $group=Group::all();
        if($group)
            return response()->json($group);
        return response()->json("failed");

    }

    public function deleteGroup(Request $request){
        $delete =Group::find($request->id);
        if($delete) {
            $delete->delete();
            return response()->json("delete group successfully");
        }
        return response()->json("failed");
    }

}
