<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserGroup_Model extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'user_group';
}
