<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group_Model extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table='groups';

    public function users(){
        return $this->belongsToMany(User::class);
    }

    public function files(){
        return $this->hasMany(Files_::class);
    }
}
